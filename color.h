// reset
#define T_RESET			"\033[0m"

// style
#define T_BRIGHT			"\033[1m"
#define T_UNDERLINE		"\033[4m"
#define T_FLASH			"\033[5m"

// --------- text colors (foreground) ---------

// regular
#define T_BLACK			"\033[30m"
#define T_RED				"\033[31m"
#define T_GREEN			"\033[32m"
#define T_YELLOW			"\033[33m"
#define T_BLUE			"\033[34m"
#define T_MAGENTA			"\033[35m"
#define T_CYAN			"\033[36m"
#define T_WHITE			"\033[37m"

// bold
#define T_BBLACK			"\033[1;30m"
#define T_BRED			"\033[1;31m"
#define T_BGREEN			"\033[1;32m"
#define T_BYELLOW			"\033[1;33m"
#define T_BBLUE			"\033[1;34m"
#define T_BMAGENTA		"\033[1;35m"
#define T_BCYAN			"\033[1;36m"
#define T_BWHITE			"\033[1;37m"

// regular underline
#define T_UBLACK			"\033[4;30m"
#define T_URED			"\033[4;31m"
#define T_UGREEN			"\033[4;32m"
#define T_UYELLOW			"\033[4;33m"
#define T_UBLUE			"\033[4;34m"
#define T_UMAGENTA		"\033[4;35m"
#define T_UCYAN			"\033[4;36m"
#define T_UWHITE			"\033[4;37m"

// high intensity
#define T_IBLACK			"\033[90m"
#define T_IRED			"\033[91m"
#define T_IGREEN			"\033[92m"
#define T_IYELLOW			"\033[93m"
#define T_IBLUE			"\033[94m"
#define T_IMAGENTA		"\033[95m"
#define T_ICYAN			"\033[96m"
#define T_IWHITE			"\033[97m"

// --------- highlight colors (background) ---------

// regular
#define T_HBLACK			"\033[40m"
#define T_HRED			"\033[41m"
#define T_HGREEN			"\033[42m"
#define T_HYELLOW			"\033[43m"
#define T_HBLUE			"\033[44m"
#define T_HMAGENTA		"\033[45m"
#define T_HCYAN			"\033[46m"
#define T_HWHITE			"\033[47m"

// hight intensity
#define T_HIBLACK			"\033[100m"
#define T_HIRED			"\033[101m"
#define T_HIGREEN			"\033[102m"
#define T_HIYELLOW		"\033[103m"
#define T_HIBLUE			"\033[104m"
#define T_HIMAGENTA		"\033[105m"
#define T_HICYAN			"\033[106m"
#define T_HIWHITE			"\033[107m"

//from Eliott & Loïc