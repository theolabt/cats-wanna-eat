# Cats wanna eats

but they hate each other

> Théo Labrosse

Dans la continuité de mes cours de programmation système, je me suis construit un exercice sur les sémaphore

## Situation

il y a un bol de nourriture dans une pièce, et une porte qui mène de cette pièce vers l'extérieur.

Quand les chats ont faim, ils vont vouloir venir entrer dans la pièce pour manger dans le bol

## règles

Un seul chat peut manger à la fois.
Quand le chat mange, un seul autre chats peut entrer dans la pièce, quand il attend pour manger il bloque la porte (aussi bien pour entrer que pour sortir)

Si d'autres chats arrivent, ils vont se mettre en file devant la porte. (bloquant la porte de l'extérieur)

Si un chat attend trop longtemps pour manger, il va partir

Si c'est le chat qui attend à l'intérieur qui veut partir, il ne pourra pas partir si des chats sont de l'autre côté de la porte

Quand le chat qui mange finit de manger, il va vouloir sortir, mais ne pourra pas sortir à cause du chat devant la porte (à l'intérieur) qui bloque le passage.
On a donc le chat qui mange qui s'éloigne du bol, puis le chat qui veut manger qui s'avance vers le bol, laissant libre la porte pour le chat qui mangeais.

Dans le cas où il y aurait des chats de l'autre côté de la porte (extérieurs) le chat qui vient de manger pourra sortir même s'ils bloquent la porte (car il vient de manger il a plus d'énergie)

C'est lorsque le chat qui mangeait sort qu'un nouveau chat peut entrer.
